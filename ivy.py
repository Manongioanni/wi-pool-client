from datetime import datetime as dt, timedelta as td
from typing import List, Optional
import asyncio, logging, os, re

import bleak
from bleak import BleakClient, BleakScanner
from bleak.backends.characteristic import BleakGATTCharacteristic
from bleak.backends.device import BLEDevice
from pytoolbox import console, logging as py_logging

log = logging.getLogger(__name__)

REG_DATA_REGEX = re.compile(r'([\da-f]{4}),\s*(.+)\s*\r\n', re.MULTILINE)

NAME_REG = '0008'
IS_LOCKED_REG = '000b'
MOTORS_POSITION_RAW_REG = '0002'
MOTORS_POSITION_PERCENT_REG = '002d'
MOTORS_MOVEMENT_RAW_REG = '0034'

# Remaining registries {'0066': 'CONNECTION PERDUE NORMALE !!!! '} from move_down() call
# Remaining registries {'0064': '0,0,0,1,0,2,1,1 '} -> from move_down() call
# TODO if is_locked then prevent actions other than unlock


def setup_logging(*logs, level: str = 'DEBUG'):
    for _log in logs + (log, ):
        py_logging.setup_logging(_log, console=True, colorize=True, level=level)


class IVYMobileFloor(object):

    DEVICE_NAME = 'IVY'
    # FRAME_SIZE = 128
    # SERVICE_UUID = 'F000C0E0-0451-4000-B000-000000000000'
    CHARACTERISTIC_UUID = 'F000C0E1-0451-4000-B000-000000000000'
    IS_LOCKED_MAP = {'0': False, '1': True}

    # TODO Adjust delays
    DEFAULT_READ_TIMEOUT = 1  # in seconds
    MOVE_READ_TIMEOUT = 0.5   # in seconds
    COMMAND_READ_DELAY = 0.5  # in seconds

    def __init__(self, *, motors_count: int, total_depth: float, water_offset: float):

        # Check arguments
        if motors_count not in range(4, 8 + 1):
            raise ValueError(f'Motors count should be in range [4,8]')

        # Attributes
        self.name: Optional[str] = None
        self.is_locked: Optional[bool] = None
        self._position: Optional[float] = None
        self.refreshed_at: Optional[dt] = None
        self.moved_at: Optional[dt] = None
        self.total_depth = total_depth
        self.water_offset = water_offset
        self.motors_count = motors_count
        self.motors_position_percent: List[float] = []
        self.motors_position_raw: List[float] = []
        self.motors_movement_raw: List[float] = []

        # Bluetooth Device and Data
        self.device: BLEDevice = None
        self.client: BleakClient = None
        self.queue: asyncio.Queue = asyncio.Queue()

    def __str__(self):
        return os.linesep.join([
            f'Mobile Floor {self.name or "<name>"}',
            f'Is Locked = {self.is_locked}',
            f'Position = {self.position:.1f} %',
            f'Refreshed At = {self.refreshed_at}',
            f'Moved At = {self.moved_at}',
            f'Total Depth = {self.total_depth} cm',
            f'Water Depth = {self.water_depth} cm',
            f'Water Offset = {self.water_offset} cm',
            f'Motors Count = {self.motors_count}',
            f'Motors Position % = {self.motors_position_percent}',
            f'Motors Position Raw = {self.motors_position_raw}',
            f'Motors Movement Raw = {self.motors_movement_raw}'
        ])

    # Context Manager

    async def __aenter__(self):
        await self.find_and_set_device()
        await self.ensure_connected()
        return self

    async def __aexit__(self, exc_t, exc_v, exc_tb):
        await self.ensure_disconnected()

    # Properties

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        if self._position is not None and value != self._position:
            self.moved_at = dt.now()
        self._position = value
        return self._position

    @property
    def water_depth(self):
        return self.total_depth - self.water_offset

    # Bluetooth I/O

    @staticmethod
    async def find_device_by_name(device_name: str, max_retries: int = 10):
        log.info(f'Searching the Bluetooth device {device_name} ...')
        for retry in range(max_retries):
            log.debug(f'[Attempt {retry+1} of {max_retries}] Finding device {device_name}')
            device = await BleakScanner.find_device_by_name(device_name)
            if device:
                return device
            await asyncio.sleep(1)
        raise RuntimeError(f'Cannot find the Bluetooth device with name {device_name}')

    async def find_and_set_device(self):
        self.device = await self.find_device_by_name(self.DEVICE_NAME)
        self.client = BleakClient(self.device.address)

    async def ensure_connected(self, max_retries: int = 10):
        if not self.client.is_connected:
            log.info(f'Connecting to device {self.device.name}')
            for i in range(max_retries):
                try:
                    await self.client.connect()
                except bleak.exc.BleakDBusError:
                    await asyncio.sleep(1)
                else:
                    break
            await self.start_notify()

    async def ensure_disconnected(self):
        if self.client.is_connected:
            log.info(f'Disconnecting from device {self.device.name}')
            await self.stop_notify()
            await self.client.disconnect()

    async def start_notify(self) -> None:
        await self.client.start_notify(self.CHARACTERISTIC_UUID, self._notification_handler)

    async def stop_notify(self) -> None:
        await self.client.stop_notify(self.CHARACTERISTIC_UUID)

    async def send_command(
        self,
        command: bytes, *,
        retries: int = 1,
        required: set = set(),
        read_timeout: Optional[float] = None
    ) -> bytes:

        registries = {}
        for retry in range(retries):
            log.debug(f'Sending command {command!r}')
            await self.ensure_connected()
            await self.client.write_gatt_char(self.CHARACTERISTIC_UUID, command + b'\r\n')
            await asyncio.sleep(self.COMMAND_READ_DELAY)
            # FIXME handle bleak.exc.BleakError: Not connected -> to reconnect
            data = await self._read_data(timeout=read_timeout or self.DEFAULT_READ_TIMEOUT)
            registries = {d[0]: d[1] for d in REG_DATA_REGEX.findall(data.decode('utf-8'))}
            if required.issubset(set(registries.keys())):
                break  # All required registries are available
        else:
            raise RuntimeError('Unable to retrieve required data')

        # Refresh floor attributes
        if raw := registries.pop(NAME_REG, None):
            self.name = raw
            log.debug(f'Refreshed name to {self.name}')
        if raw := registries.pop(IS_LOCKED_REG, None):
            self.is_locked = self.IS_LOCKED_MAP[raw]
            log.debug(f'Refreshed is_locked to {self.is_locked}')
        if raw := registries.pop(MOTORS_POSITION_PERCENT_REG, None):
            self.motors_position_percent = [float(v) for v in raw.split(',')]
            self.refreshed_at = dt.now()
            log.debug(f'Refreshed motors position % to {self.motors_position_percent}')
        if raw := registries.pop(MOTORS_POSITION_RAW_REG, None):
            self.motors_position_raw = [float(v) for v in raw.split(',')]
            log.debug(f'Refreshed motors position raw to {self.motors_position_raw}')
        if raw := registries.pop(MOTORS_MOVEMENT_RAW_REG, None):
            self.motors_movement_raw = [float(v) for v in raw.split(',')]
            log.debug(f'Refreshed motors movement raw to {self.motors_movement_raw}')

        # Refresh floor position
        if self.motors_position_percent:
            positions = self.motors_position_percent[-self.motors_count:]
            if len(positions) < self.motors_count:
                log.warning(f'There are {len(positions)} motors, expected {self.motors_count}')
            self.position = sum(positions) / len(positions)
        else:
            self.position = None

        if registries:
            log.warning(f'Remaining registries {registries}')

        return data

    # Status

    def moved_recently(self, max_delay: td) -> bool:
        return self.moved_at is not None and (dt.now() - self.moved_at) < max_delay

    def refreshed_recently(self, max_delay: td) -> bool:
        return self.refreshed_at is not None and (dt.now() - self.refreshed_at) < max_delay

    async def refresh_status(self) -> None:
        log.info('Refresh Status')
        await self.send_command(b'name')    # Retrieve device name
        await self.send_command(b'rd 152')  # Retrieve is locked status
        await self.refresh_position()

    async def refresh_position(self) -> None:
        """
        Inspired by: mobile-app/decode/client-client-module-es2015.js#2928
        """
        log.info('Refresh Position')
        await self.send_command(b'ssor 1', retries=2, required={MOTORS_POSITION_PERCENT_REG})

    def _get_motor_index(self, number: int) -> int:
        if not (1 <= number <= self.motors_count):
            raise ValueError(f'Wrong motor number {number}')
        index = number + len(self.motors_position_percent) - self.motors_count
        log.debug(f'Map motor n°{number} to index {index}')
        assert index - 1 in range(len(self.motors_position_percent))
        return index

    # Operations

    async def move_down(self) -> bytes:
        """
        Inspired by: mobile-app/decode/cycle-cycle-module-es2015.js#L221
        """
        log.info('Move Down')
        return await self.send_command(b'downc', read_timeout=self.MOVE_READ_TIMEOUT)

    async def move_up(self) -> bytes:
        """
        Inspired by: mobile-app/decode/cycle-cycle-module-es2015.js#L224
        """
        log.info('Move Up')
        return await self.send_command(b'upc', read_timeout=self.MOVE_READ_TIMEOUT)

    async def set_position(self, position: float):
        target_position = max(0, min(position, 100))  # Overflow
        log.info(f'Set position to {target_position}')

        # Ensure tray position is known
        await self.refresh_position()
        assert self.position is not None  # For mypy

        # Move up or down to reach target position
        if self.position > target_position:
            log.info(f'Moving floor up to position {target_position}')
            while self.position > target_position:
                self.show_position(target=target_position)
                await self.move_up()
                # TODO Ensure to break loop if target will be never reachable (e.g. wrong reading)
                # TODO Compute position delta and set refreshed maximum delay proportionally
                # TODO The closer the more frequently we need to know if we reached the target
                if not self.refreshed_recently(max_delay=td(seconds=5)):
                    await self.refresh_position()

        elif self.position < target_position:
            log.info(f'Moving floor down to position {target_position}')
            while self.position < target_position:
                self.show_position(target=target_position)
                await self.move_down()
                # TODO Ensure to break loop if target will be never reachable (e.g. wrong reading)
                # TODO Compute position delta and set refreshed maximum delay proportionally
                # TODO The closer the more frequently we need to know if we reached the target
                if not self.moved_recently(max_delay=td(seconds=5)):
                    await self.refresh_position()

        # Ensure its really closed or opened :)
        if target_position == 0:
            await self.move_up()
        elif target_position == 100:
            await self.move_down()

    def show_position(self, target: Optional[float] = None) -> None:
        if target is None:
            raise NotImplementedError()
        else:
            log.info(f'Position : {self.position} % -> {target} %')

    async def set_pool_depth(self, depth: float):
        return await self.set_position((depth + self.water_offset) / self.total_depth)

    async def stop_motors(self) -> bytes:
        """
        Inspired by: mobile-app/decode/client-client-module-es2015.js#L2931
        """
        log.info('Stop Motors')
        await self.send_command(b'ok')
        return await self.send_command(b'ok')

    async def adjust_motor_down(self, number: int) -> bytes:
        """
        Inspired by: mobile-app/decode/init-init-module-es2015.js#L TODO
        """
        log.info(f'Adjust Motor {number} Down by 0.3')
        index = self._get_motor_index(number)
        return await self.send_command(f'dw1 m{index} 0.3'.encode('utf-8'))

    async def adjust_motor_up(self, number: int) -> bytes:
        """
        Inspired by: mobile-app/decode/init-init-module-es2015.js#L612
        """
        log.info(f'Adjust Motor {number} Up by 0.3')
        index = self._get_motor_index(number)
        return await self.send_command(f'up1 m{index} 0.3'.encode('utf-8'))

    async def set_threshold(self, ask_confirm: bool = True) -> bool:
        """
        Inspired by: mobile-app/decode/client-client-module-es2015.js#L5487
        """
        log.info('Set Threshold')
        if ask_confirm and not console.confirm('Do you want to set (up) threshold?'):
            log.warning('Action set threshold cancelled by user')
            return False

        await self.send_command(b'iholdup')
        return True

    async def unlock_tray(self, ask_confirm: bool = True) -> bool:
        """
        Inspired by: mobile-app/decode/admin-admin-module-es2015.js#L681
        """
        if ask_confirm and not console.confirm('Do you want to unlock the tray?'):
            log.warning('Action unlock tray cancelled by user')
            return False

        # TODO why sending commands twice ?
        for i in range(2):
            await self.send_command(b'wr 0 152')  # TODO what is this?
            await self.send_command(b'unlock')    # TODO what is this?
        return True

    async def _read_data(self, *, timeout: float) -> bytes:
        data = bytearray()
        while True:  # Consume all data available
            try:
                data += await asyncio.wait_for(self.queue.get(), timeout)
            except asyncio.TimeoutError:
                return bytes(data)

    async def _notification_handler(
        self,
        characteristic: BleakGATTCharacteristic,
        data: bytes
    ) -> None:
        """Simple notification handler which enqueue incoming data."""
        channel = characteristic.description
        data = bytes(data)
        log.debug(f"Received data from {channel}: {data}")  # type: ignore[str-bytes-safe]
        await self.queue.put(data)
