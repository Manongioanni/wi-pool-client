#!/usr/bin/env python

from typing import Optional
import asyncio, code, logging, os

import ivy
from pytoolbox import console

log = logging.getLogger(__name__)

ACTIONS = STATUS, CLOSE, OPEN, SET_POS, DOWN, UP, STOP, ADJUST, UNLOCK, SHELL, EXIT = [
    'status', 'close', 'open', 'set-pos', 'down', 'up', 'stop', 'adjust', 'unlock', 'shell', 'exit'
]

MOTOR_ACTIONS = [DOWN, UP, EXIT]

MY_CONFIG = {
    'motors_count': 6,
    'total_depth': 166,
    'water_offset': 20
}

MY_MAP = """
--------------Haie---------------
Coin herbe | 4  5  3  | (arbre)
           | 2  1  6  | (pergola)
-------------Maison--------------
"""


async def main() -> None:
    async with ivy.IVYMobileFloor(**MY_CONFIG) as floor:
        await floor.refresh_status()
        print(floor)
        while True:
            action = console.choice('Floor > Action', ACTIONS)
            if action == STATUS:
                await floor.refresh_status()
                print(floor)
            elif action == CLOSE:
                await floor.set_position(0)
            elif action == OPEN:
                await floor.set_position(100)
            elif action == SET_POS:
                await action_set_position(floor)
            elif action == DOWN:
                await floor.move_down()
            elif action == UP:
                await floor.move_up()
            elif action == STOP:
                await floor.stop_motors()
            elif action == ADJUST:
                await action_adjust_motors(floor)
            elif action == UNLOCK:
                await floor.unlock_tray()
            elif action == SHELL:
                code.interact(local={**globals(), **locals()})
            elif action == EXIT:
                return
            else:
                raise NotImplementedError(action)


async def action_adjust_motors(floor: ivy.IVYMobileFloor) -> bool:
    while True:
        print(MY_MAP)
        motor = ask_number('Floor > Adjust Motor > Enter motor number', 1, floor.motors_count)
        if motor is None:
            return await floor.set_threshold()
        floor._get_motor_index(motor)
        while True:
            action = console.choice(f'Floor > Adjust Motor > {motor} > Action', MOTOR_ACTIONS)
            if action == DOWN:
                await floor.adjust_motor_down(motor)
            elif action == UP:
                await floor.adjust_motor_up(motor)
            elif action == EXIT:
                break
            else:
                raise NotImplementedError(action)


async def action_set_position(floor: ivy.IVYMobileFloor) -> None:
    position = ask_number('Floor > Position > Enter desired position', 0, 100)
    if position is not None:
        await floor.set_position(position)


def ask_number(message: str, minimum: int, maximum: int) -> Optional[int]:
    while True:
        try:
            value = input(f'{message} [{minimum}-{maximum} or exit]: ')
            if value == 'exit':
                return None
            number = int(value)
            if minimum <= number <= maximum:
                return number
        except TypeError:
            pass
        log.error(f'Please enter a valid number between [{minimum}, {maximum}]')


if __name__ == '__main__':
    ivy.setup_logging(log, level=os.getenv('LOG_LEVEL', 'INFO'))
    asyncio.run(main())
