# Wi Pool Client

Client développé afin de me permettre de commander mon fond mobile.

Peut-être même constituer une interface standalone avec Kivy pour le rendre immédiatement portable sur plusieurs plateformes dont le mobile.

Après tout la partie contrôle devrait se faire uniquement depuis un périphérique qui est à proximité pour des questions de sécurité.
Et l'application mobile développée par Wi Pool répondait à cette contrainte.

A la base je souhaitais faire une intégration à Home Assistant mais je trouve l'idée du contrôle depuis n'importe où trop risquée.

## Disclaimer

Le code est partagé selon le modèle Open-Source et ne me rend pas responsable de ce que vous allez en faire ni des conséquences de son utilisation.

## Bonne Conduite

Merci de partager avec tous vos apports en tant que merge request.
Je me charge de faire la revue de code et le travail d'intégration.

## Prérequis

* Un ordinateur avec le Bluetooth
* Git
* Python 3.11

## Installation

Cloner le projet avec Git (ou alors le télécharger).

Dans un terminal (ligne de commande) :

```
git clone https://gitlab.com/wi-pool/wi-pool-client.git
cd wi-pool-client
python3.11 -m venv venv/
source venv/bin/activate
pip install -U pip setuptools wheel
pip install -r requirements.txt
```

Le programme est dorénavant prêt à être utilisé.

## Configuration

Il faut bien noter que la configuration s'effectue au niveau du code source, c'est à dire que vous allez devoir éditer le fichier [ivy_cli.py](ivy_cli.py) de votre copie locale.

Les lignes suivantes (extrait de `ivy_cli.py`) :

```python
MY_CONFIG = {
    'motors_count': 6,
    'total_depth': 166,
    'water_offset': 20
}

MY_MAP = """
--------------Haie---------------
Coin herbe | 4  5  3  | (arbre)
           | 2  1  6  | (pergola)
-------------Maison--------------
"""
```

Devront êtres mises à jour en fonction de votre fond mobile.

La variable `MY_CONFIG` contient :

* `motors_count` : Le nombre de moteurs que votre fond comporte
* `total_depth` : La profondeur totale de votre piscine (depuis le haut des margelles) quand le fond est au plus bas
* `water_offset` : La distance entre la ligne d'eau et le haut de la margelle

Ensuite vous pourrez mettre à jour `MY_MAP`, qui est affiché à titre indicatif lors de l'action d'ajustement des moteurs. Un moyen simple d'y parvenir étant de faire ajuster un moteur (par exemple le numéro 1) et voir quel moteur bouge puis mettre à jour le contenu de `MY_MAP`.

## Utilisation

Activer le bluetooth sur votre ordinateur et le placer à proximité du récepteur Bluetooth de votre fond mobile.

Ouvrir un terminal dans l'emplacement du code source que vous avez précédemment téléchargé et configuré.

```
cd wi-pool-client
source venv/bin/activate
python ivy_cli.py
```

Le programme vous affichera un menu interatif avec lequel vous allez interagir en lançant des commandes ...

* `status` : Récupérer le statut du fond mobile (position, etc)
* `close` : Remonter le fond mobile tout en haut (fermer la piscine)
* `open` : Descendre le fond mobile tout en bas (ouvrir la piscine)
* `set-pos` : Définir une position en pourcentage (`0` = fermé, `100` = ouvert à 100 %)
* `down` : Descendre d'un incrément le fond mobile
* `up` : Monter d'un incrément le fond mobile
* `stop` : S'assurer que le fond mobile est arrêté (jamais utilisé)
* `adjust` : Ajuster le positionnement des moteurs (comporte un sous-menu)
* `unlock` : Débloquer le fond mobile (la fonctionnalité clé pour pas mal d'entre vous)
* `shell` : Un shell Python interactif pour les geeks
* `exit` : Sortir du programme ou d'un sous-menu
